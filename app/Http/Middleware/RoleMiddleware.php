<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->isSuperAdmin()){
          return $next($request);
        }elseif (Auth::user()->isAdmin() && ($request->route()->Uri()=='route-2' || $request->route()->Uri()=='route-3')) {
          return $next($request);
        }elseif (Auth::user()->isGuest() && ($request->route()->Uri()=='route-3')) {
          return $next($request);
        }else{
          abort(403);
        }

    }
}
