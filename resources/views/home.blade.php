@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in
                </div>
            </div>
        </div>
    </div>
    <div class="jumbotron align-center">
      <div class="container">
      <a class = "btn btn-primary" href="/route-1">GO TO ROUTE-1</a>
      <a class = "btn btn-primary" href="/route-2">GO TO ROUTE-2</a>
      <a class = "btn btn-primary" href="/route-3">GO TO ROUTE-3</a>
    </div>
    </div>
</div>
@endsection
